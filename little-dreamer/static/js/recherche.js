// merci beaucoup à https://palant.info/2020/06/04/the-easier-way-to-use-lunr-search-with-hugo/
// et https://lord.re/posts/206-recherche-pour-un-blog-statique/
window.addEventListener("DOMContentLoaded", function(event)
    {
        var index = null;
        var lookup = null;
        var queuedTerm = null;

        var form = document.getElementById("search");
        var input = document.getElementById("search-input");

        form.addEventListener("keyup", function(event)
            {
                event.preventDefault();

                var term = input.value.trim();
                if (!term)
                    return;

                startSearch(term);
            }, false);
        form.addEventListener("submit", function(event)
            {
                event.preventDefault();

                var term = input.value.trim();
                if (!term)
                    return;

                startSearch(term);
            }, false);

        function startSearch(term)
        {
            if (index)
            {
                // Index already present, search directly.
                search(term);
            }
            else if (queuedTerm)
            {
                // Index is being loaded, replace the term we want to search for.
                queuedTerm = term;
            }
            else
            {
                // Start loading index, perform the search when done.
                queuedTerm = term;
                initIndex();
            }
        }

        function searchDone()
        {
            queuedTerm = null;
        }

        function initIndex()
        {
            var request = new XMLHttpRequest();
            request.open("GET", "/index.json");
            request.responseType = "json";
            request.addEventListener("load", function(event)
                {
                    lookup = {};
                    index = lunr(function()
                        {
                            // Uncomment the following line and replace de by the right language
                            // code to use a lunr language pack.
                            this.use(lunr.fr);

                            this.ref("uri");

                            // If you added more searchable fields to the search index, list them here.
                            this.field("title");
                            this.field("content");
                            this.field("tags");

                            for (var doc of request.response)
                            {
                                this.add(doc);
                                lookup[doc.uri] = doc;
                            }
                        });

                    // Search index is ready, perform the search now
                    search(queuedTerm);
                }, false);
            request.addEventListener("error", searchDone, false);
            request.send(null);
        }

        function search(term)
        {
            var results = index.search(term);

            // The element where search results should be displayed, adjust as needed.
            var ul = document.querySelector(".liste-resultats");
            ul.innerHTML = "";


            var nb_res = document.querySelector(".nombre-resultats")
            var title = document.createElement("p");
            if (results.length == 0)
                title.textContent = `Aucun résultat pour “${term}”`;
            else if (results.length == 1)
                title.textContent = `1 résultat pour “${term}”`;
            else
                title.textContent = `${results.length} résultats pour “${term}”`;
            nb_res.innerHTML = title.textContent;

            var template = document.getElementById("search-result");
            for (var result of results)
            {
                var doc = lookup[result.ref];

                var li = document.createElement("li");
                let elemlink = document.createElement('a');
                elemlink.innerHTML = doc.title;
                elemlink.setAttribute('href', doc.uri + '#:~:text=' + encodeURI(term));
                li.appendChild(elemlink);

                ul.appendChild(li);
            }
            //title.scrollIntoView(true);

            searchDone();
        }

    }, false);

